# gaudible

> Single-file program that makes notifications audible for Gnome.

![screenshot](/screenshot.png)

## Usage

```bash
# all filters
./gaudible.py -v

# only specific filters
./gaudible.py -v --filter calendar --filter calendar-legacy

# register specific sounds for specific filters
./gaudible.py \
    -v \
    --sound calendar:calendar.oga \
    --sound firefox:browser.oga   \
    --sound default-sound.oga        # sound for everything else
```

## Why?

I got tired of missing meetings because Evolution doesn't play audio
for appointment reminders by default.

[This](https://gitlab.gnome.org/GNOME/evolution/issues/152) doesn't
seem to have any traction, but maybe one day
[this](https://gitlab.gnome.org/GNOME/glib/issues/1340) becomes a
thing...

## How?

All this does is listen for common notification traffic patterns on
DBus.

gaudible's dependencies are highly likely to already be installed by
default on modern Linux desktop distros, but just in case they're
not:

```bash
# debian 12+
sudo apt install python3 python3-dbus python3-gi pipewire-bin

# fedora 40+
sudo dnf install python3 python3-dbus python3-gobject pipewire-utils
```


## Reference

```bash
# see gdbus usage
cat test.sh

# listen for dbus messages (https://dbus.freedesktop.org/doc/dbus-specification.html#message-bus-routing-match-rules)
dbus-monitor 'type=method_call,member=AddNotification' \
             'type=method_call,member=Notify'

# identify dbus unique addresses (same as qdbus, qdbus-qt5, qdbus-qt6)
qdbus-qt6

# describe dbus service methods
qdbus-qt6 org.gtk.Notifications /org/gtk/Notifications

# resolve name to a dbus address (e.g., :1.23)
qdbus-qt6 org.freedesktop.DBus /org/freedesktop/DBus org.freedesktop.DBus.GetNameOwner org.gtk.Notifications
```
